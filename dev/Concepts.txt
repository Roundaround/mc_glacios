Old ideapad: http://roundaround.titanpad.com/1

Credits
    #tropicraft
    Biomes O Plenty

Creatures
    Ice Elemental
    Arctic Wyrm
        Small, very fast
        Latches onto player
        Slowly drains health, hunger, or warmth
    Goat
    Mammoth
    Will o' Whisp
    Grick
    Illithid
    Drake

Mechanics
    Cold
        Works similarly to hunger
        Certain types of armor slow drain
        Water in Glacios rapidly speeds up
        Fire blocks increase, torches maintain
        Multiple configurable difficulties
        Easy: No cold meter.
        Normal: It's like a second hunger bar.
        Hard: Faster and more punishing, but nothing especially vicious or unique.
        Hardcore: THIS IS THE REAL GLACIOS! When you're cold, you move slower, you get hungry faster, 
        and you take more damage. Don't freeze. :)
    Longer Nights

Environment
    Huge cave systems (similar to Nether)
    (Possibly) higher height limit
        Would be for compensating for larger cave systems
    Ice Lens
        Pockets of frozen water underground.
    BIOMES (Bases):
        Extreme Hills
        Floating Island
        Lake
        Ocean
        Plains
        Plateau
        Taiga
        Underworld
        Volcano
        
Blocks
    Trees:
        Life/Soul
            Pale Blue Color, Glows, Emits Particles
        Taiga
    Foliage/Bushes/Flora:
        Life/Soul Vine
        Razor Grass
        Some kind of bush that covers large area and slows entities (not as much as web)
    Base Blocks:
        Slate
        Ash
        Ash Stone
        White Obsidian
        Crystal Water
            Super cold water - counter to lava.  Heats into white obsidian
        Prism Shard
            Crystalline shard that emits light.  Randomly chooses color
            Possible colors are blue, red, green, and purple.
        Gelisol
            A classification of soil that contain permafrost, a layer of soil near the surface that is frozen year-round.
    
